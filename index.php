<?php $con =new  mysqli("localhost", "root", "", "viajefeliz");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/css/bootstrap.min.css">
    
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/carousel.css">
    <link rel="stylesheet" href="./css/bar.css">
    <link rel="stylesheet" href="./css/css/style.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <title>VIAJEFELIZ.com</title>

 
</head>
<body>
  <!------------------------------------------------Menu viaje feliz---------------------------------------------------------------------->
  
  <div class="container">
    <header>
      <nav class="navbar navbar-expand-md navbar-ligth  fixed-top text-white" style="position: absolute;">
         <div id="logotipo">
             <img src="img/logoo.svg" width="208" height="97"
             alt="Haz clic aquí para volver a la página de inicio">
         </div>
  
     <button class="navbar-toggle " type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
        </button>
  
        <div class="collapse navbar-collapse" id="navbarCollapse">
           
  
            <form class="form-inline mt-2 mt-md-0">
             <a href="#actualidad" class="nav-link text-white"  >
                 VIAJE FELIZ SAS
                </a> 
  <!-------------------------------------------------------------Servicios----->
            <div class="dropdown">
               <a href="#servicios" class="nav-link text-white"  style="cursor:pointer">
               Servicios
                </a>
             </div>
  <!------------------------------------------------------------trayectoria----->
             <div class="dropdown">
               <a href="clientes.html" class="nav-link text-white"  style="cursor:pointer">
                 Trayectoria
                </a>
               </div>
  
    <!------------------------------------CONTACTO ----------------------------------------------------->
              <div class="contacto">
               <a href="#contacto" class="nav-link text-white"  style="cursor:pointer">
                 <i class="fas fa-comments"></i>Contáctanos
                </a>
              </div>
            </form>
        </div>
     </nav>
  </header>
  </div>
  
    <!-----------------------------------------------------------Video playas del mundo fuente YOUTUBE-------------------------------------------------------------->
     <main role="main">
   
       <div class="carousel slide" id="myCarousel" data-ride="corousel">
           
           <div class="carousel-inner">
               <div class="carousel-item active" style="display: contents;">
                <video class="video-fluid" autoplay loop muted style="width: 100%;">
                    <source src="./media/viajefeliz.mp4" type="video/mp4" />
                    
                  </video>  
                  <div class="container">
                    <div class="carousel-caption">
                        <h1>Agencia de VIAJEFELIZ SAS</h1>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium modi cumque dolore laborum culpa minima excepturi? Cupiditate odio commodi neque a, quod quos, at voluptates dignissimos odit quasi facilis omnis?</p>
                        <p><a href="#" role="button" class="btn btn-lg btn-primary">Conocenos</a></p>
                     </div>
                 </div>
                </div>
                
           </div>
       </div>
  <!-----------------------------------------------------------FORMULARIO DE BUSQUEDA-------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------------------------------------------->
         
       <div class="container">
        <div class="text-center">
            <h1>VIAJE FELIZ S.A.</h1><br>
            <form action="" class="form-group" method="POST">
              <div class="form-inline">
                <div class="col-sm-5 text-center">
                  <span class="btn btn-danger">Elige tu destino nosotros te llevamos</span><hr>
                  <input type="text" name="barras" placeholder="Escribe tu destino ideal nosotros te llevamos" autofocus required class="form-control"><br>
                
                </div>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">Fecha de llegada</span><hr>
                  <input type="date" name="ini" placeholder="ini" autofocus required class="form-control">
                </div>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">Fecha de partida</span><hr>
                  <input type="date" name="fin" placeholder="fin" autofocus required class="form-control">
                </div>
                <div class="col-sm-1 text-center">
                  <span class="btn btn-danger">Numero de personas</span><br><hr>
                  <input type="text" name="personas" placeholder="Nª personas" autofocus required class="form-control">
                
                </div>
              </div>  
              <hr> <hr>

               <input type="submit" name="Buscar" value="Buscar" class="btn btn-primary">
                <input type="reset" name="Cancelar" value="Cancelar" class="btn btn-info">
           
            
              <table class="table table-hover">
               <tbody>
                <tr>
                
                <?php   
                   $ubi = $_POST['barras'];
                   $per = $_POST['personas'];
                   $fecha1= new DateTime($_POST['ini']);
                   $fecha2= new DateTime($_POST['fin']);
                   $diff = $fecha1->diff($fecha2);
                   if(isset($_POST['barras'])){
                     
                    $query="SELECT * FROM alojamiento WHERE ubicacion='$ubi' and estado_aloj = 1 and no_personas='$per'" ;
                    $res=mysqli_query($con, $query);
                  }
                     while($row=mysqli_fetch_array($res)){?>
                     <div class="">
                     <div class="card card-hotel d-flex flex-column justify-content-between">
                       <p>
                       <?php echo '<h3 class="card-title" data-toggle="tooltip" data-placement="top" title="Los mejores platos, el mejor precio"> '.$row['ubicacion'].' </h3>'?>
                       <?php echo '<img src="'.$row['foto'].'" alt="Responsive image" class="img-thumbnail">'?>
                       <?php echo '<p> -Ubicacion '.$row['barrio'].' '.$row['descripcion']. '</p>'?>
                       <?php echo '<p style="visibility: collapse;" name="id">'.$row['id_alojamiento'].' </p>'?>
                       <?php if($row['estado_aloj'] ==1) echo '<p>-Estado del alojamiento Disponible</p>'?>
                       <?php echo '<h3>-Costo del alojamiento por noche:  '.$row['costo'].' </h3>'?>
                       <?php echo '<h3>' .$diff->days * $row['costo']. ' costo total por ' .$diff->days.' dias </h3>';?>
                       <?php echo '<p>-Numero de personas por habitacion  '.$row['no_personas'].' </p>'?>
                       <?php if($row['tipo_aloj'] ==1) echo '<p>-Casa en la ciudad</p>'?>
                       <?php if($row['tipo_aloj'] ==2) echo '<p>-Cabaña en los alrededores de la ciudad</p>'?>
                       <?php if($row['mascotas'] ==1) echo '<p>-Se permiten mascotas</p>'?>
                       <?php if($row['mascotas'] ==2) echo '<p>-No se permiten mascotas</p>'?>
                       </p>
                       <a href="reserva.php?id=<?php echo $row['id_alojamiento']" class="form-control bg-danger text-center text-white"><span class="oi oi-plus" style="color: black;"></span>Contactar</a>
                       
                    </div>
                     </div>
                  </tr>
                <?php } ?>
               </tbody>
            
              </table>
              
             <hr> 
        </form>
        </div>
      </div>
       <!----------------------------------------------------Secciones Quienes somos---------------------------------------------------->
       <div class="container marketing">
           <h1 id="actualidad" style="padding-top: 65px; text-align: center;">Viaje feliz</h1>
            <hr>
           <div class="row">
               <div class="col-md-6">
                <img src="./img/casas-rural/cabaña5.jpg" alt="First slide" style="width: 40rem;">
                <h1 style="text-align: center; color: #fd7e14;"><br>
                  <span class="Single7" style="font-size: 50px;transform: translate(-50%, -50%); line-height: 1.2; margin: 0 0 35px; padding: 0 0 25px;color: #2f7de1;     font-weight: bold; text-align: center;">132632</span><br>
                  Viajes realizados en zonas rurales</h1>
              </div>
               <div class="col-md-6">
                <img src="./img/casas-ciudad/casa1.jpeg" alt="First slide" style="width: 29rem;">
                <h1 style="text-align: center; color: #fd7e14;"><br>
                 <span class="Single7" style="font-size: 50px;transform: translate(-50%, -50%); line-height: 1.2; margin: 0 0 35px; padding: 0 0 25px;color: #2f7de1;     font-weight: bold; text-align: center;">132632</span><br>
                 Viajes realizados en zonas urbanas</h1>
              </div>
           </div>
  <hr>
<!--------------------------------------------------------------Nosotros------------------------------------------------------------------------->

<!------------------------------------------------------------Galeria nosostros---------------------------------------------------------------------------->             

<!----------------------------------------------------HISTORIA------------------------------------------------------------------------>
          </div> 
 <!--------------------------------------------------------------------Comentarios-------------------------------------------------------------------------------------------->          
           <div class="container text-center">
            <h1 style="padding-top: 40px;">Conocenos a traves de nuestros usuarios</h1>
              <hr>
              <figure class="highcharts-figure">
                 <div id="cont">
                
                </div>
             </figure>
           </div>
           <hr>                     
<!--------------------------------------------------------------------Indicadores generales-------------------------------------------------------------------------------------------->          
<h1 style="text-align: center;">Nuestros numeros lo confirman</h1><br>                  
<div class="row center" id="saladDIV" style="text-align: -webkit-center;">
            
             <div class="col-md-4">
               <img src="./img/1green-150x150.png" alt="">
               <p style="text-align: center;"><span style="color: #339966;"><strong>AHORRO CO2:</strong></span><br>
                <span class="Single" style="font-size: 50px;transform: translate(-50%, -50%); line-height: 1.2; margin: 0 0 35px; padding: 0 0 25px;color: #2f7de1;     font-weight: bold;">150000</span><br>
                Kg dejados de emitir a la admosfera</p>
             </div>

             <div class="col-md-4">
              <img src="./img/2blue-150x150.png" alt="">
              <p style="text-align: center;"><span style="text-align: left; color: #339966;"><strong>AHORRO DE TIEMPO:</strong><br>
              </span><span class="Single2" style="font-size: 50px; transform: translate(-50%, -50%);   line-height: 1.2; margin: 0 0 35px;  padding: 0 0 25px; color: #2f7de1;  font-weight: bold;">45000</span>
                 <br><span style="text-align: left;">Horas en transporte publico</span></p>
              
            </div>
            <div class="col-md-4">
              <img src="./img/3yellow-150x150.png" alt="">
              <p style="text-align: center;"><span style="text-align: left; color: #339966;"><strong>AHORRO DE DINERO:</strong><br>
              </span><span class="Single3" style=" font-size: 50px;  transform: translate(-50%, -50%);line-height: 1.2;  margin: 0 0 35px;padding: 0 0 25px;  color: #2f7de1;  font-weight: bold;">100</span>
              <br><span style="text-align: left;">Millones en pasajes de bus</span></p>
            </div>
            <div class="col-md-4">
              <img src="./img/4cyan-150x150.png" alt="">
              <p style="text-align: center;"><strong style="text-align: left;"><span style="color: #339966;">CALIDAD DE VIDA:</span></strong>
                <br> <span class="Single4" style="  font-size: 50px; transform: translate(-50%, -50%);line-height: 1.2;  margin: 0 0 35px; padding: 0 0 25px;color: #2f7de1; font-weight: bold;">150</span><span style="text-align: left;"><br>
                % de mejora en la calidad de vida</span></p>
            </div>

            <div class="col-md-4">
              <img src="./img/5rosado-150x150.png" alt="">
              <p style="text-align: center;"><span style="text-align: left; color: #339966;"><strong>IMAGEN:</strong><br>
              </span>
              <span class="Single5" style=" font-size: 50px;transform: translate(-50%, -50%);line-height: 1.2;  margin: 0 0 35px; padding: 0 0 25px;  color: #2f7de1;  font-weight: bold;">70</span>
              <br><span style="text-align: left;">Empresas que le han apostado a nuestro proyecto</span></p>
              
            </div>
            <div class="col-md-4">
              <img src="./img/6lightGreen-150x150.png" alt="">
              <p style="text-align: center;"><span style="text-align: left; color: #339966;"><strong>SOSTENIBILIDAD:</strong></span>
                <br><span class="Single6" style="font-size: 50px; transform: translate(-50%, -50%); line-height: 1.2;margin: 0 0 35px;padding: 0 0 25px;    color: #2f7de1;font-weight: bold;">25000</span>
                    <br><span style="text-align: left;">
                Aporte a la sociedad y al planeta</span></p>
                
            </div>
           </div>
           <hr class="featurette-divider">
       </div>
      <!---------------------------------------------------tweeter SNIPET--------------------------------------------------------------->
      <hr>
        <div class="container">
          <div class="row">
            <div class="col-md-4">
            <blockquote class="twitter-tweet"><p lang="es" dir="ltr">Viaje feliz, agencia turística especializada en alojamientos vacacionales en ciudades y sus periferias.</p>&mdash; wilva (@william50445105) <a href="https://twitter.com/william50445105/status/1292267031534993408?ref_src=twsrc%5Etfw">August 9, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>    
          </div>
            <div class="col-md-4">
            <blockquote class="twitter-tweet"><p lang="es" dir="ltr">Visita la Colombia rural después de la cuarentena con los planes que tenemos para ti, VIAJEFELIZ SAS</p>&mdash; wilva (@william50445105) <a href="https://twitter.com/william50445105/status/1292267394916913153?ref_src=twsrc%5Etfw">August 9, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>   
            </div>
            <div class="col-md-4">
            <blockquote class="twitter-tweet"><p lang="es" dir="ltr">Estamos en la buenas y en las malas agencia VIAJEFELIZ donara el 100% de sus ingresos para ayudar al personal medico y dará 1 semana gratis a todos aquellos que respeten la cuarentn,todos a cuidarnos<br>-Este tuit hace parte de un proyecto académico su contenido es pedagogico.</p>&mdash; wilva (@william50445105) <a href="https://twitter.com/william50445105/status/1292268357258010627?ref_src=twsrc%5Etfw">August 9, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>    
            </div>
          </div>
        </div>
      <hr>
      <!--------------------------------------------------------Modal---------------------------------------------------------->
       <div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
           <div role="document" class="modal-dialog modal-dialog-centered">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h5 id="ModalLongTitle" class="modal-title">
                             Administrador
                         </h5>
                         <form action="#" class="form-group">
                           <input class="form-control" type="text" name="nombre" placeholder="Ingrese su correo" ><br>
                           <input class="form-control" type="password" name="pass" placeholder="Ingrese su contraseña"><br>
                           <input class="btn btn-primary" type="submit" name="Enviar" value="Enviar"><br>
                         </form>
                     </div>
                 </div>
           </div>
       </div>
    </main>
<!-------------------------------------------------------------------------------------------------------------------------->
<!-----------------------------------------FORMULARIO Y MAPA--------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------------------------------------->
    <div class="container">
      <div class="col-md-12">
        <iframe src="https://www.google.com/maps/d/embed?mid=1_OGnq0aJTyOrhmoecxW8xswaAX157hP_" width="100%" height="600rem"></iframe>
        <hr>
      </div>
    </div>
<!------------------------------------------------------------------------------------------------------------------------------------->
<!-------------------------------------------------------Footer------------------------------------------------------------------------------>
<!------------------------------------------------------------------------------------------------------------------------------------->
<footer class="footer bg-primary">
  <div class="row">
    <div class="col-md-2 text-white">
     <p style="padding: 25px;"> CONTACTO
      VIAJE FELIZ S.A
      ♦ Calle 88A # 88 - 88
      ✆ (+57) 313-846-7817
      ✉ comunicacion@viajefeliz.com
      Política tratamiento de datos</p>
    </div>
    <div class="col-md-8">
      <ul class="list">
        <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="https://www.youtube.com" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
        <li><a href="https://api.whatsapp.com/send?phone=573138467817&text=Hola!%20En%20un%20momento%20te%20responderemos.%20" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
        <li><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        </ul>
    </div>
    <div class="col-md-2">
       <img src="./img/logo-B-08-e1495835389499.png" alt="">
    </div>
  </div>
</footer>


<!---------------------------------------------------------CHATBOOT TIDIO---------------------------------------------------------------------------->
<script src="//code.tidio.co/mg1xqkit9q830pn01nozrkhwjmikjrtt.js" async></script>
<!---------------------------------------------------SCRIPTS---------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------->

<script type="text/javascript">
  $({ Counter: 0 }).animate({
  Counter: $('.Single').text()
}, {
  duration: 100000,
  easing: 'swing',
  step: function() {
    $('.Single').text(Math.ceil(this.Counter));
  }
});
</script>

<script type="text/javascript">
  $({ Counter: 0 }).animate({
  Counter: $('.Single2').text()
}, {
  duration: 100000,
  easing: 'swing',
  step: function() {
    $('.Single2').text(Math.ceil(this.Counter));
  }
});
</script>
 
<script type="text/javascript">
  $({ Counter: 0 }).animate({
  Counter: $('.Single3').text()
}, {
  duration: 100000,
  easing: 'swing',
  step: function() {
    $('.Single3').text(Math.ceil(this.Counter));
  }
});
</script>

<script type="text/javascript">
  $({ Counter: 0 }).animate({
  Counter: $('.Single4').text()
}, {
  duration: 100000,
  easing: 'swing',
  step: function() {
    $('.Single4').text(Math.ceil(this.Counter));
  }
});
</script>


<script type="text/javascript">
  $({ Counter: 0 }).animate({
  Counter: $('.Single5').text()
}, {
  duration: 100000,
  easing: 'swing',
  step: function() {
    $('.Single5').text(Math.ceil(this.Counter));
  }
});
</script>

<script type="text/javascript">
  $({ Counter: 0 }).animate({
  Counter: $('.Single6').text()
}, {
  duration: 100000,
  easing: 'swing',
  step: function() {
    $('.Single6').text(Math.ceil(this.Counter));
  }
});
</script>

<script type="text/javascript">
  $({ Counter: 0 }).animate({
  Counter: $('.Single7').text()
}, {
  duration: 100000,
  easing: 'swing',
  step: function() {
    $('.Single7').text(Math.ceil(this.Counter));
  }
});
</script>

<script src="./js/grafica.js" type="text/javascript" name="grafica">

  </script>
    
    <script src="../webside/code/highcharts.js"></script>
    <script src="../webside/code/modules/wordcloud.js"></script>
    <script src="../webside/code/modules/exporting.js"></script>
    <script src="../webside/code/modules/export-data.js"></script>
    <script src="../webside/code/modules/accessibility.js"></script>
    <script src="./js/progg.js"></script>
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="../webside/js/loading-bar.js"></script>
    <script src="./css/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
</body>
</html>
<!------------------------------------------------Comentarios de los usuarios-------------------------------------------------------------------------------------------------->
<!DOCTYPE HTML>
<html>
	<head>

		<style type="text/css">
.highcharts-figure, .highcharts-data-table table {
    width: 100%;
    margin: 1em auto;
}

.highcharts-data-table table {
    font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 3.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 1800;
    padding: 1.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
    size: 10rem;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

#text{
  size: 12rem;
}
		</style>
	</head>
	<body>

<script type="text/javascript">
var text = 'Gracias MeB Adriana perez, Gracias son los mejores Felipe Montalvo, A pedalear con toda Josue Rodrigues, Que buen servicio Maria Lopez, Gracias Muchachos Camilo Polo, Quiero seguir rodando con ustedes Luis Arango, Es un buen servicio Jorge Martinez, Buen programa Andres Jimenez, Gracias Valeria Torres, Gracias Maria Camila, Gracias amigos por su iniciativa Julian Palacios, Sigan moviendo el mundo en bici Paula, Los mejores Ivan Vargas, Que gran servicio David Perez, He mejorado mi rendimiento fisico Katherin Pinto, Gracias MeB Adriana perez, Gracias son los mejores Felipe Montalvo, A pedalear con toda Josue Rodrigues, Que buen servicio Maria Lopez, Gracias Muchachos Camilo Polo, Quiero seguir rodando con ustedes Luis Arango, Es un buen servicio Jorge Martinez, Buen programa Andres Jimenez, Gracias Valeria Torres, Gracias Maria Camila, Gracias amigos por su iniciativa Julian Palacios, Sigan moviendo el mundo en bici Paula, Los mejores Ivan Vargas, Que gran servicio David Perez, He mejorado mi rendimiento fisico Katherin Pinto, Gracias MeB Adriana perez, Gracias son los mejores Felipe Montalvo, A pedalear con toda Josue Rodrigues, Que buen servicio Maria Lopez, Gracias Muchachos Camilo Polo, Quiero seguir rodando con ustedes Luis Arango, Es un buen servicio Jorge Martinez, Buen programa Andres Jimenez, Gracias Valeria Torres, Gracias Maria Camila, Gracias amigos por su iniciativa Julian Palacios, Sigan moviendo el mundo en bici Paula, Los mejores Ivan Vargas, Que gran servicio David Perez, He mejorado mi rendimiento fisico Katherin Pinto, Gracias MeB Adriana perez, Gracias son los mejores Felipe Montalvo, A pedalear con toda Josue Rodrigues, Que buen servicio Maria Lopez, Gracias Muchachos Camilo Polo, Quiero seguir rodando con ustedes Luis Arango, Es un buen servicio Jorge Martinez, Buen programa Andres Jimenez, Gracias Valeria Torres, Gracias Maria Camila, Gracias amigos por su iniciativa Julian Palacios, Sigan moviendo el mundo en bici Paula, Los mejores Ivan Vargas, Que gran servicio David Perez, He mejorado mi rendimiento fisico Katherin Pinto,  rutrum commodo mi lacus pretium erat. Phasellus pretium ultrices mi sed semper. Praesent ut tristique magna. Donec nisl tellus, sagittis ut tempus sit amet, consectetur eget erat. Sed ornare gravida lacinia. Curabitur iaculis metus purus, , rutrum commodo mi lacus pretium erat. Phasellus pretium ultrices mi sed semper. Praesent ut tristique magna. Donec nisl tellus, sagittis ut tempus sit amet, consectetur eget erat. Sed ornare gravida lacinia. Curabitur iaculis metus purus, eget pretium est laoreet ut. Quisque tristique augue ac eros malesuada, vitae facilisis mauris sollicitudin. Mauris ac molestie nulla, vitae facilisis quam. Curabitur placerat ornare sem, in mattis purus posuere eget. Praesent non condimentum odio. Nunc aliquet, odio nec auctor congue, sapien justo dictum massa, nec fermentum massa sapien non tellus. Praesent luctus eros et nunc pretium hendrerit. In consequat et eros nec interdum. Ut neque dui, maximus id elit ac, consequat pretium tellus. Nullam vel accumsan lorem.';
var lines = text.split(/[,\.]+/g),
    data = Highcharts.reduce(lines, function (arr, word) {
        var obj = Highcharts.find(arr, function (obj) {
            return obj.name === word;
        });
        if (obj) {
            obj.weight += 1;
        } else {
            obj = {
                name: word,
                weight: 1
            };
            arr.push(obj);
        }
        return arr;
    }, []);

Highcharts.chart('cont', {
    accessibility: {
        screenReaderSection: {
            beforeChartFormat: '<h1>{chartTitle}</h1>' +
                '<div>{chartSubtitle}</div>' +
                '<div>{chartLongdesc}</div>' +
                '<div>{viewTableButton}</div>'
        }
    },
    series: [{
        type: 'wordcloud',
        data: data,
        name: 'Comentarios'
    }],
    title: {
        text: ''
    }
});
		</script>
	</body>
</html>
