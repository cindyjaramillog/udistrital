
$(document).ready(function() {
    var title = {
        text: 'Bici Usuarios'   
    };
    var subtitle = {
         text: 'MeB'
    };
    var xAxis = {
        categories: ['2011', '2013', '2014', '2015', '2016', '2017'
               ,'2018', '2019', '2020']
    };
    var yAxis = {
       title: {
          text: 'Temperature (\xB0C)'
       },
       plotLines: [{
          value: 0,
          width: 1,
          color: '#808080'
       }]
    };   
 
    var tooltip = {
       valueSuffix: '\xB0Usuarios',
     
    }
 
    var legend = {
       layout: 'vertical',
       align: 'right',
       verticalAlign: 'middle',
       borderWidth: 0
    };
 
    var series =  [
       {
          name: 'Usuarios',
          data: [0.0, 50.0, 300.0, 1000.0, 1820.0, 3180.0, 9352.0,
             12152.0, 16336.0],
             
       }
       
    ];
 
    var json = {};
 
    json.title = title;
    json.subtitle = subtitle;
    json.xAxis = xAxis;
    json.yAxis = yAxis;
    json.tooltip = tooltip;
    json.legend = legend;
    json.series = series;
 
    $('#contar').highcharts(json);
 });