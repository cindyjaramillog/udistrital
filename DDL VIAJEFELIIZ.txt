-- BASES AVANZADAS --------------------- VIAJEFELIZ
create database VIAJEFELIZ;
use VIAJEFELIZ;

-- -------------- alojamiento
-- ---------- tipo alojamiento 1 casa, 2 cabaña
-- ---------- estado 1 disponible, 2 reservado, 3 alquilado
-- ---------- Mascotas 1 se permite, 2 no se permite
-- ---------- tipo ventilacion 1 aire acondicionado, 2 calefaccion, 3 ninguno
 
create table alojamiento(
 id_alojamiento int(11) not null,
 tipo_aloj int(11) not null,
 estado_aloj int(11) not null,
 costo decimal(10,5) not null,
 no_habitaciones int(11) not null,
 no_banos int(11) not null, 
 no_personas int(11) not null,
 barrio varchar(30) not null,
 ubicacion varchar(30) not null,
 mascotas int(11) not null,
 tipo_ventilacion int(11) not null,
 foto varchar(100) not null,
 descripcion varchar(100) not null,
 primary key(id_alojamiento)
);
--------------------------------------------------------------------------------------------------------

-- -----------------------------------------------------------------------------------------------------------
create table usuario(
id_usuario int(11) auto_increment,
nombre varchar(50) not null,
direccion varchar(50) not null,
telefono varchar(20) not null,
nacionalidad varchar(20) not null,
primary key(id_usuario)
);

-- --------------------------------------------------------------------------------------------------------------

create table asignaciones(
id_asignacion int(11) auto_increment,
pago decimal(10,5) not null,
start_date datetime,
end_date datetime,
temporada int(11) not null,
id_aloj int(11) not null,
id_user int(11) not null,
FOREIGN KEY(id_aloj) REFERENCES alojamiento(id_alojamiento),
FOREIGN KEY(id_user) REFERENCES usuario(id_usuario),
primary key(id_asignacion)
);
-- ---------------------------------------------------------------------------------------------------------------

create table encuesta(
id_encuesta int(11) auto_increment,
id_usu int(11) not null,
fecha_encuesta datetime,
calificacion int(11)not null,
FOREIGN KEY(id_usu) REFERENCES usuario(id_usuario),
primary key(id_encuesta)
);
